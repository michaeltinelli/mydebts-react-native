import React from 'react';
import { Provider } from 'react-redux'
import { View, StyleSheet } from 'react-native'
import { createStoreWithFirebase } from './src/config/firebaseConfig'
import AppContainer from './src/navigation/Navigation'
import rootReducer from './src/main/reducers'
import { mainBackGroundColor } from './src/styles/colors';

const store = createStoreWithFirebase(rootReducer)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
          <AppContainer />
      </Provider>
    );
  }
}



