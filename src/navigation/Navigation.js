import React from 'react'
import { createStackNavigator ,createAppContainer } from 'react-navigation'

import TopMenu from '../components/TopMenu'

import LoginScreen from '../screens/Login'
import HomeScreen from '../screens/Home'
import SaveDebtScreen from '../screens/SaveDebt'
import {blackColor} from '../styles/colors';

const AppNavigator = createStackNavigator({
    /*
    Login: {
        name: 'Login',
        screen: LoginScreen,
        navigationOptions: {
            title: 'Acesso'
        }
    },
    */
    Home: {
        name: 'Home',
        screen: HomeScreen,
        navigationOptions: {
            title: 'Home',
        }
    },
    CadDebt: {
        name: 'CadDebt',
        screen: () => <SaveDebtScreen isEdit={false} />,
        navigationOptions: {
            title: 'Cadastro Dívida'
        }
    },
    SaveDebt: {
        name: 'SaveDebt',
        screen: () => <SaveDebtScreen isEdit={true} />,
        navigationOptions: {
            title: 'Edição Dívida'
        }
    },

}, {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
       headerStyle: {
           backgroundColor: blackColor,
           borderBottomColor: '#fff200',
           borderBottomWidth: 2,
       },
       headerTitleStyle: {
           color: '#fff'
       }
    }
    
})


export default createAppContainer(AppNavigator)