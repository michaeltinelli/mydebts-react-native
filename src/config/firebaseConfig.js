import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import { compose } from 'redux'
import { reduxFirestore } from 'redux-firestore'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import {  reactReduxFirebase } from 'react-redux-firebase'

const fbConfig = {
    apiKey: "AIzaSyB8b6mGAvLxIrTSZsTlvY_gNMIyqIWPyOA",
    authDomain: "projectspersonal-32c68.firebaseapp.com",
    databaseURL: "https://projectspersonal-32c68.firebaseio.com",
    projectId: "projectspersonal-32c68",
    storageBucket: "projectspersonal-32c68.appspot.com",
    messagingSenderId: "291644263998"
}

const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true 
}

firebase.initializeApp(fbConfig)

firebase.firestore()
firebase.auth()

export const createStoreWithFirebase = compose(
    applyMiddleware(thunk),
    reduxFirestore(firebase),
    reactReduxFirebase(firebase)
)(createStore)



