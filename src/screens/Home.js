import React, { Component } from 'react'
import { View, StyleSheet, Text, FlatList, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { getDebts, setInitialValues, deleteDebt, loadDebt } from '../store/actions/actionsDebts'
import Debt from '../components/Debt'
import { Icon } from 'react-native-elements';
import TopMenu from '../components/TopMenu';


class Home extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <Icon type={'font-awesome'} name={'plus-circle'} size={30} color={'#fff'} 
                containerStyle={{ marginRight: 4 }} onPress={() => { navigation.navigate('CadDebt'), setInitialValues('', 0) }} />
            )
        }
    }

    componentDidMount = () => {
        this.props.onGetList()
    }

    saveDebt = values => {
        console.log(values)
    }

    deleteDebt = (debt) => {
        this.props.onRemoveDebt(debt)
    }
    
    goToEditScreen = (debt) => {
        setInitialValues(debt.name, debt.price)
        this.props.onLoadDebt(debt)
        this.props.navigation.navigate('SaveDebt')
    }

    render() {

        return (
            <View style={styles.container}>
                <TopMenu />
                    {
                        this.props.debts.length > 0 ? <FlatList data={this.props.debts} keyExtractor={item => item.id}
                        renderItem={({ item }) => <Debt bgColor={''} onSaveDebt={this.saveDebt} 
                        onGoToEditScreen={this.goToEditScreen} debt={{...item}} onDeleteDebt={this.deleteDebt} />} 
                        /> 
                        : <Text>Nenhum registro de dívida</Text>
                    }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
       
    },
   
})

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
       onGetList: () => dispatch(getDebts()),
       onRemoveDebt: debt => dispatch(deleteDebt(debt)),
       onLoadDebt: debt => dispatch(loadDebt(debt))
    }
}

const mapStateToProps = ({ debts }, ownProps) => {
    return {
        debts: debts.debts
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home)