import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { mainBackGroundColor } from '../styles/colors'
import { InputDefault } from '../components/InputFields'
import { Field, reduxForm, reset } from 'redux-form'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { User } from '../model/models'

import { registerOrLoginUser, verifyIfHasUserOnline, logout, loadUser } from '../store/actions/actionsLogin'
import moment from 'moment';

class Login extends Component {

    state = {
        isLogin: true,
        isLogoff: false,
    }

    componentDidMount = () => {
        //console.log(moment(new Date()).format('DD/MM/YYYY'))
        verifyIfHasUserOnline().then(resp => {
            console.log(resp)
            if(resp) {
                this.setState({ isLogoff: true })
                this.props.onLoadUser()
            } else {
                this.setState({ isLogoff: false })
            }
        })
    }

    loginOrRegister = values => {
        const { onRegisterOrLogin } = this.props

        User.name = values.name
        User.email = values.email
        User.password = values.password
        User.hasOnline = true
        onRegisterOrLogin(User)

    }
    
    renderForm = () => {
        
        const { handleSubmit, valid} = this.props
       
        return(
            <View style={styles.container}>
                { (!this.state.isLogin) && <Field name={'name'} label={'Nome completo'} component={InputDefault}  /> }
                <Field name={'email'} label={'Email'} keyboard={'email-address'} component={InputDefault}  />
                <Field name={'password'} label={'Senha'}  secure={true} maxLength={10} 
                component={InputDefault}  />

                <Button disabled={!valid} buttonStyle={{backgroundColor: '#3c3c3d', }} containerStyle={{ margin: 10, borderRadius: 4, }} 
                title={this.state.isLogin ? 'Entrar' : 'Registrar'} onPress={handleSubmit(this.loginOrRegister)}/>

                <Text style={styles.textChangeMode} onPress={() => this.setState({isLogin: !this.state.isLogin})}>
                {`Clique aqui para se ${this.state.isLogin ? 'registrar' : 'logar'}`}
                </Text>
            </View>
        )
    }

    renderButton = () => (
        <View >
            <Button title={'Fazer logoff'} buttonStyle={{ backgroundColor: '#c10000'}} 
            titleStyle={{color: '#fff', fontWeight: 'bold',}} onPress={() => { this.props.onLogout(),  this.setState({isLogoff: false})}} />
        </View>
    )

    render() {
       
        if(this.props.submitSucceded) {
            this.setState({ isLogoff: true })
        }
        return(
            <View style={styles.container}>
                { this.state.isLogoff ?  this.renderButton() : this.renderForm()  }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 20,
    },
    textChangeMode: {
        color: 'black',
    }
})

const mapDispatchToProps = dispatch => {
   return {
       onRegisterOrLogin: (user) => dispatch(registerOrLoginUser(user)),
       onLogout: () => dispatch(logout()),
       onLoadUser: () => dispatch(loadUser())
   }
}



const validate = values => {
    
    const errors = {}

    if (values.name && values.name.length < 6) {
        errors.name = 'Preencha o campo nome com no mínimo 6 letras.'
    }
    
    if (!values.email) {
        errors.email = 'Preencha o campo e-mail.'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Ex: maria@algo.com'
    }

    if (!values.password) {
        errors.password = 'Preencha o campo senha.'
    } else if (values.password.length < 6) {
        errors.password = 'Preencha o campo senha com no mínimo 6 letras.'
    } else if (values.password.length > 20) {
        errors.password = 'Preencha o campo senha no maximo 20 letras.'
    }

    return errors
}

Login = reduxForm({form: 'LoginForm', validate })(Login)

export default connect(null, mapDispatchToProps)(Login)