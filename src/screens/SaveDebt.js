import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { mainBackGroundColor } from '../styles/colors'
import { connect } from 'react-redux'
import { InputDefault, InputWithMask } from '../components/InputFields'
import { Field, reduxForm, reset } from 'redux-form'
import { Button } from 'react-native-elements'
import { MaskService } from 'react-native-masked-text'
import { Debts } from '../model/models'
import { saveDebts, initialValues , setInitialValues} from '../store/actions/actionsDebts'


class SaveDebt extends Component {

    componentDidMount = () => {
        console.log(this.props.debt)
    }

    save = values => {
        const { reset } = this.props
        const realPrice = MaskService.toRawValue('money', values.price)
        
        if(this.props.debt) {
            const debt = {
                id: this.props.debt.id,
                name: values.name,
                price: realPrice
            }
            this.props.onSaveDebts(debt, this.props.isEdit)
            setInitialValues(debt.name, debt.price)
        } else {
            Debts.name = values.name
            Debts.price = realPrice
            this.props.onSaveDebts(Debts, this.props.isEdit)
        }

        reset()
    }
    
    renderForm = () => {

        const { handleSubmit, submitting, valid } = this.props

        return (

            <View style={styles.container}>

                <Field name={'name'} label={'Nome'} component={InputDefault} />
                <Field name={'price'} label={'Preço'} keyboard={'numeric'} options={{
                    precision: 2,
                    separator: ',',
                    delimiter: '.',
                    unit: 'R$',
                    suffixUnit: ''
                }} component={InputWithMask} />

                <Button disabled={!valid || submitting} buttonStyle={{ backgroundColor: '#3c3c3d', }} 
                    containerStyle={{ margin: 10, borderRadius: 4, }} title={this.props.isEdit ? 'Salvar' : 'Cadastrar'} 
                    onPress={handleSubmit(this.save)} />

            </View>
        )
    }

    
    render() {
       

        return (
            <View style={styles.container}>
               {this.renderForm()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 20,
    },
   
})

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
       onSaveDebts: (debt, isEdit) => dispatch(saveDebts(debt, isEdit)),
    }
}

const mapStateToProps = ({ debts }, ownProps) => {
    return {
       debt: debts.debt
    }
}

const validate = values => {
    
    const errors = {}

    if (!values.name) {
        errors.name = 'Preencha o campo nome.'
    } else if (values.name.length < 6) {
        errors.name = 'Preencha o campo com no mínimo 6 letras.'
    }
    
    if (!values.price) {
        errors.price = 'Preencha o campo preço.'
    }
    
    return errors
}

SaveDebt = reduxForm({form: 'SaveDebtForm', validate, initialValues })(SaveDebt)

export default connect(mapStateToProps, mapDispatchToProps)(SaveDebt)