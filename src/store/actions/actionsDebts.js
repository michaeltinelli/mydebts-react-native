import firebase from 'firebase'
import { DEBT_SAVED, DEBT_REMOVED, DEBTS_LIST, DEBT_EDITED, DEBT_LOADED } from '../actionTypes/actionsTypes'
import { DEBTS_COLLECTION } from '../../main/collectionsName'
import { showMessage } from '../../main/functions'


const firestoreRef = firebase.firestore().collection(DEBTS_COLLECTION)

export let initialValues = {
    name: '',
    price: 0,
}

export function setInitialValues(name, price) {
    initialValues.name =  name || ''
    initialValues.price = price || 0
}

export function saveDebts(debt, isEdit) {
    return dispatch => {
        debt.updatedAt = new Date()
       
        if(isEdit) {
            firestoreRef.doc(debt.id).set({...debt})
            .then(_ => { dispatch({ type: DEBT_EDITED, payload: debt }), 
            showMessage(`Dívida salva com sucesso`)})
            .catch(_ => showMessage(`Erro ao salvar dívida`))
        } else {
            firestoreRef.add({ ...debt })
            .then(resp => { debt.id = resp.id ,dispatch({ type: DEBT_SAVED, payload: debt }), showMessage(`Dívida cadastrada com sucesso`) })
            .catch(_ => showMessage(`Erro ao cadastrar dívida`))   
        }

    }
}

export function getDebts() {
    return (dispatch) => {
       
        firestoreRef.get().then(resp => {
           
            dispatch({ type: DEBTS_LIST, payload: resp.docs.map(d =>  {
                let debt = {
                    id: d.ref.id,
                    name: d.data().name,
                    price: d.data().price,
                    updatedAt: d.data().updatedAt
                }
                return debt
            })})
        }).catch(_ => showMessage('Erro ao carregar as dívidas.'))  
    
    }
}

export function loadDebt(debt) {
    return dispatch => dispatch({type: DEBT_LOADED, payload: debt})
}

export function deleteDebt(debt) {
    return disptach => {
        firestoreRef.doc(debt.id).delete()
        .then(_ => disptach({ type: DEBT_REMOVED, payload: debt.id }))
        .catch(_ => showMessage(`Erro ao deletar a divida ${debt.name}.`))

    } 
}