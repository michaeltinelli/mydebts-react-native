import firebase from 'firebase'
import { USER_LOGGED, USER_LOGOFF } from '../actionTypes/actionsTypes'
import { DEBTS_COLLECTION } from '../../main/collectionsName'
import { messageErrorOnFirebaseAuth, showMessage } from '../../main/functions'


export const authRef = firebase.auth()
const firestoreRef = firebase.firestore().collection(DEBTS_COLLECTION)

export function registerOrLoginUser(user) {
    return (dispatch) => {
       
        if(user.name) {
            registerUserOnAuthentication(user.email, user.password).then(resp => {
                user.uid = resp.user.uid
                user.createdAt = new Date()
                firestoreRef.add({
                    ...user
                }).then(_ => { dispatch({ type: USER_LOGGED, payload: user })})
                .catch(err => messageErrorOnFirebaseAuth(err.code))
    
            }).catch(err => messageErrorOnFirebaseAuth(err.code))
        } else {
            login(user.email, user.password).then(resp => {
                loadUser()
            }).catch(err => messageErrorOnFirebaseAuth(err.code))
        }
    }
}

function registerUserOnAuthentication(email, password) {
    return authRef.createUserWithEmailAndPassword(email, password)
}

export function verifyIfHasUserOnline() {
    return new Promise(resolve => {
        authRef.onAuthStateChanged(user => {
            if(user) {
                resolve(true)
            } else {
                resolve(false)
            }
        })
    })
}

function login(email, password) {
    return authRef.signInWithEmailAndPassword(email, password)
}

export function loadUser() {
    return dispatch => firestoreRef.limit(1).where('email', '==', authRef.currentUser.email).get().then(resp => {
        resp.forEach(u => dispatch({ type: USER_LOGGED, payload: u.data() }))
    })
}

export function logout() {
    return dispatch => {
        authRef.signOut().then(_ => dispatch({type: USER_LOGOFF}))
        .catch(err => showMessage('Não foi possível efetuar logoff. Tente mais tarde!')) 
    }
}


