import { USER_LOGGED } from '../actionTypes/actionsTypes'

const initialState = {
    user: null,

}

export default (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGGED:
            return { ...state, user: action.payload }
        case USER_LOGGED:
            return { ...state, user: null }    
        default:
           return state;
    }
}