import { DEBT_SAVED, DEBTS_LIST, DEBT_REMOVED, DEBT_EDITED, DEBT_LOADED } from '../actionTypes/actionsTypes'

const initialState = {
    debts: [],
    debt: null,
    total: 0,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case DEBT_SAVED:
            return { 
                ...state, 
               debts: state.debts.concat({...action.payload})
            }
        case DEBTS_LIST:
            return {
                ...state,
                debts: action.payload
            }
        case DEBT_REMOVED: 
            return {
                ...state,
                debts: state.debts.filter(debt => debt.id !== action.payload)
            }
        case DEBT_EDITED:
            return {
                ...state,
                debts: state.debts.map(d => {
                    if(d.id === action.payload.id) {
                        d = {...action.payload}
                    }
                    return d
                })
            }
        case DEBT_LOADED:
            return {
                ...state,
                debt: action.payload
            }               
        default:
           return state;
    }
}