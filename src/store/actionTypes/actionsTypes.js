export const USER_LOGGED = 'USER_LOGGED'

export const USER_LOGOFF = 'USER_LOGOFF'

export const DEBT_SAVED = 'DEBT_SAVED'

export const DEBT_EDITED = 'DEBT_EDITED'

export const DEBT_REMOVED = 'DEBT_REMOVED'

export const DEBT_LOADED = 'DEBT_LOADED'

export const DEBTS_LIST = 'DEBTS_LIST'