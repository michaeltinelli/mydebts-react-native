import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import moment from 'moment'
import { MaskService } from 'react-native-masked-text'

export default props => {

    console.log(props.debt)
    const price = MaskService.toMask('money', props.debt.price)
    const dateCad = moment(props.debt.createdAt).format('DD/MM/YYYY')

    return (
        <View style={[styles.container, { backgroundColor: props.bgColor || '' }]}>
            <View style={styles.textsContainer}>
                <Text>Nome: {props.debt.name}</Text>
                <Text style={[styles.text, { color: (props.debt.price <= 500) ? 'green' : 'red' }]}>{price}</Text>
               
                <Text style={{color: 'black'}}>{dateCad}</Text>
                
            </View>

            <View style={styles.iconsContainer}>
                <Icon containerStyle={{marginRight: 5}} type={'font-awesome'} size={25} color={'black'} name={'pencil'} 
                onPress={() => props.onGoToEditScreen(props.debt)} />
                <Icon type={'font-awesome'} size={25} color={'black'} name={'trash'} onPress={() => props.onDeleteDebt(props.debt)} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5,
        width: '85%',
        borderBottomWidth: 2,
        borderBottomColor: 'black',
    },
    textsContainer: {
        justifyContent: 'center', 
        alignItems: 'flex-start',
    },
    iconsContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'flex-end', 
        marginRight: 50,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 12,
    }
})