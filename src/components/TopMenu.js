import React,{ Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Icon, Avatar } from 'react-native-elements'
import { ImagePicker } from 'expo'
import { blackColor } from '../styles/colors'
import moment from 'moment'

export default class TopMenu extends Component  {

    state = {
        uri: null
    }

    takePicture = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
          });
      
          if (!result.cancelled) {
            this.setState({ uri: result.uri });
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.textDate}>{moment(new Date()).locale('pt-br')
                .format('dddd, D [de] MMMM [de] YYYY')}</Text>
    
                <Avatar containerStyle={{borderBottomColor: '#fff200', borderBottomWidth: 1,}} size={30} onPress={() => this.takePicture() } rounded 
                source={{ uri: this.state.uri }} showEditButton />
            </View>
        )
    }


    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#017fad',
        padding: 6,
        width: '100%',
        marginBottom: 4,
    },
    textDate: {
        color: '#fff',
        fontWeight: 'bold',
    }
})