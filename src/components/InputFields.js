import React from 'react'
import { View, StyleSheet, Text, TextInput, Dimensions  } from 'react-native'
import { Icon } from 'react-native-elements'
import { TextInputMask } from 'react-native-masked-text'

export const InputDefault = ({ input, label, maxLength, keyboard, secure, meta: { touched, error }, })  => (
    <View style={styles.container}>
        { touched && error && <Text style={styles.errorStyle}>{error}</Text>  }

        <TextInput style={[styles.input]} {...input} value={input.value} onChangeText={input.onChange} 
            placeholder={label} maxLength={maxLength || 40} keyboardType={keyboard || 'default'}
            secureTextEntry={false || secure}
            />
    </View>
)

export const InputWithMask = ({ input, label, maxLength, keyboard, type, options, meta: { touched, error }, })  => (
    <View style={styles.container}>
        { touched && error && <Text style={styles.errorStyle}>{error}</Text>  }

        <TextInputMask style={[styles.input]} {...input} value={input.value} onChangeText={input.onChange} 
            placeholder={label} maxLength={maxLength || 40} keyboardType={keyboard || 'default'} type={type ||'money'}
            options={options || {}}
        />
    </View>
)

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        margin: 5,
    },
   
    input: {
        backgroundColor: '#3c3c3d',
        width: Dimensions.get('screen').width - 25,
        borderRadius: 6,
        color: '#FFF',
        padding: 6,
    },
    errorStyle: {
        fontSize: 14,
        color: '#FFF',
        fontWeight: 'bold',
        backgroundColor: '#ad0000',
        padding: 6,
        
    },
    textToggle: {
        color: '#fff',
        fontWeight: 'bold',
        marginLeft: 4,
    }
})