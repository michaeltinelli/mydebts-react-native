import {  ToastAndroid } from 'react-native'


export function messageErrorOnFirebaseAuth(code) {
  
  console.log(code)

  let message =  null

    if (code === 'auth/app-deleted' || code === 'auth/app-not-authorized'
    || code === 'auth/argument-error' 
    || code === 'auth/invalid-api-key' 
    || code === 'auth/operation-not-allowed'
    || code === 'auth/requires-recent-login' 
    || code === 'auth/too-many-requests' 
    || code === 'auth/unauthorized-domain' 
    || code === 'auth/web-storage-unsupported') {

      message = 'Erro ao entrar no aplicativo. Tente mais tarde.'
    } else if (code === 'auth/invalid-user-token' || code === 'auth/user-token-expired') {
      message = 'Acesse com seu login novamente.'
    } else if (code === 'auth/network-request-failed') {
      message = 'Problema de conexão. Tente novamente mais tarde.'
    } else if (code === 'auth/user-disabled') {
      message = 'Sua conta foi desabilidata.'
    } else if (code === 'auth/email-already-in-use') {
      message = 'O Email digitado ja possui cadastro.'
    } else if(code === 'auth/user-not-found') {
      message = 'Usuário não encontrado.'
    } else if(code === 'auth/wrong-password') {
      message = 'Senha incorreta.'
    }
    
  ToastAndroid.showWithGravity(`${message}`, ToastAndroid.LONG, ToastAndroid.TOP) 
}

export function showMessage(message) {
    ToastAndroid.showWithGravity(`${message}`, ToastAndroid.LONG, ToastAndroid.TOP)
}

