import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'
import { reducer as formReducer } from 'redux-form'

import userReducer from '../store/reducers/userReducer'
import debtsReducer from '../store/reducers/debtsReducer'

export default rootReducer = combineReducers({
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    form: formReducer,
    user: userReducer,
    debts: debtsReducer,  
})
