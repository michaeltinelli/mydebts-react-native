export const User = {
    uid: '',
    name: '',
    email: '',
    password: '',
    phone: '',
    hasOnline: false,
    createdAt: null,
}

export const Debts = {
    id: '',
    name: '',
    price: 0,
    updatedAt: null,
}